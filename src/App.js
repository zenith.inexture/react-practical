import Icons from "./Practicals/Icons";
import RatingFunctionality from "./Practicals/RatingFunctionality";
import ReactChart from "./Practicals/ReactChart";
import ReactColor from "./Practicals/ReactColor";
import ReactCountUp from "./Practicals/ReactCountUp";
import ReactCreditCards from "./Practicals/ReactCreditCards";
import ReactDatePicker from "./Practicals/ReactDatePicker";
import ReactIdleTimer from "./Practicals/ReactIdleTimer";
import ReactLoading from "./Practicals/ReactLoading";
import ReactModal from "./Practicals/ReactModal";
import ReactVideoPlayer from "./Practicals/ReactVideoPlayer";
import ToastNotification from "./Practicals/ToastNotification";
import ToolTip from "./Practicals/ToolTip";

function App() {
  return (
    <div>
      {/* <Icons /> */}
      {/* <ToastNotification /> */}
      {/* <ReactModal /> */}
      {/* <ToolTip /> */}
      {/* <ReactCountUp /> */}
      {/* <ReactColor /> */}
      {/* <ReactCreditCards /> */}
      {/* <ReactDatePicker /> */}
      {/* I have added the mdx deck for the preasntation and added the script for the run the preasentation*/}
      {/* <ReactVideoPlayer /> */}
      {/* <ReactLoading /> */}
      {/* <ReactChart /> */}
      {/* <ReactIdleTimer /> */}
      <RatingFunctionality />
    </div>
  );
}

export default App;
