import React from "react";
import { FaReact } from "react-icons/fa";
//this react-icons is pkg for the react-icons
//here the fa is font awesome icons
//if we use the another icons then have only change the import and the name of the icons
import { AiFillAndroid } from "react-icons/ai";
import { IconContext } from "react-icons";

//if we have to use the same color and same size through-out the project then react-icons is the provide the IconContext
function Icons() {
  return (
    <>
      <IconContext.Provider value={{ color: "blue", size: "100px" }}>
        {/* with the help of provider of iconContext we can pass the same value color and the size for the all icons */}
        <FaReact />
        <AiFillAndroid />
      </IconContext.Provider>
    </>
  );
}

export default Icons;
