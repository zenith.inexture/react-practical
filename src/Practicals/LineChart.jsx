import React from "react";
import { Chart as ChartJS, registerables } from "chart.js";
import { Line } from "react-chartjs-2";

//migration v4 to chartjs, so we to register all chart js components
ChartJS.register(...registerables);

function LineChart() {
  const data = {
    labels: ["jan", "fab", "mar", "apr", "may"],
    datasets: [
      {
        label: "Sales for 2020 (M)",
        data: [3, 2, 2, 1, 5],
        fill: true,
        tension: 0.5,
        borderColor: "#F88A82",
        backgroundColor: "#F88A82",
        pointBackgroundColor: "#F88A82",
        pointBorderColor: "#F88A82",
      },
      {
        label: "Sales for 2021 (M)",
        data: [2, 3, 3, 4, 6],
        borderColor: "blue",
        fill: true,
        tension: 0.5,
        borderColor: "#00EE76",
        backgroundColor: "#00EE76",
        pointBackgroundColor: "#00EE76",
        pointBorderColor: "#00EE76",
      },
    ],
  };

  const options = {
    title: {
      display: true,
      text: "Line Chart",
    },
    scales: {
      yAxes: {
        min: 0,
        max: 7,
      },
    },
  };
  return (
    <div style={{ display: "flex", justifyContent: "center", width: "720px" }}>
      <Line data={data} options={options} />
    </div>
  );
}

export default LineChart;
