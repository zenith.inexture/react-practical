import React, { useState } from "react";
import Modal from "react-modal";

Modal.setAppElement("#root");
function ReactModal() {
  const [isModalOpen, setisModalOpen] = useState(false);
  return (
    <div>
      {/* we have to define the isOpen props for open the modal */}
      {/* so, we have to controll the modal open/close manually */}
      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setisModalOpen(false)}
        style={{
          overlay: {
            backgroundColor: "gray",
          },
          content: {
            color: "orange",
          },
        }}
      >
        {/* isRequestClose is props for the modal close on the overlay/outside the modal */}
        <h1>Hello, everyone!!</h1>
        <p>this is zenith. nice to meet you!!</p>
        <button onClick={() => setisModalOpen(false)}>Close</button>
      </Modal>
      <button onClick={() => setisModalOpen(true)}>Open Modal</button>
    </div>
  );
}

export default ReactModal;
