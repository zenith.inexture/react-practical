import React, { forwardRef } from "react";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css";

//if we want a tooltip on the child componet then we have to pass the ref to the child component
const Childcompo = forwardRef((props, ref) => {
  return (
    <div ref={ref}>
      <div>Hello, Hover me</div>
    </div>
  );
});

function ToolTip() {
  return (
    <div style={{ textAlign: "center" }}>
      {/* tooltip with basic content */}
      <div style={{ marginBottom: "20px" }}>
        <Tippy
          content={"Basic tooltip"}
          arrow={false} //arrow is disappear
          delay={1000} //delay is showing and hiding the tooltip
          placement={"right"} //placement of tooltip
        >
          <button>Hover</button>
        </Tippy>
      </div>

      {/* we can pass the HTML element as well as the react component also */}
      <div style={{ marginBottom: "20px" }}>
        <Tippy
          content={<span style={{ color: "yellow" }}>Yellow Colored</span>}
        >
          <button>Hover</button>
        </Tippy>
      </div>

      <div style={{ marginBottom: "20px" }}>
        <Tippy content={<span>ToolTip on Text of child component</span>}>
          <Childcompo />
        </Tippy>
      </div>
    </div>
  );
}

export default ToolTip;
