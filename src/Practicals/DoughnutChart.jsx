import React from "react";
import { Chart as ChartJS, registerables } from "chart.js";
import { Doughnut } from "react-chartjs-2";

ChartJS.register(...registerables);

function DoughnutChart() {
  const data = {
    labels: ["jan", "fab", "mar", "apr", "may"],
    datasets: [
      {
        label: "Sales for 2020 (M)",
        data: [3, 2, 2, 1, 5],
        borderColor: "white",
        backgroundColor: [
          "rgba(255, 0, 0, 0.7)",
          "rgba(0, 255, 0, 0.7)",
          "rgba(255, 255, 0, 0.7)",
          "rgba(0, 0, 255, 0.7)",
          "rgba(128, 0, 128, 0.7)",
        ],
        hoverOffset: 10,
        borderRadius: 10,
        spacing: 10,
        hoverBackgroundColor: ["red", "green", "yellow", "blue", "purple"],
        hoverBorderColor: ["red", "green", "yellow", "blue", "purple"],
      },
    ],
  };

  const options = {
    title: {
      display: true,
      text: "Doughnut Chart",
    },
  };
  return (
    <div style={{ display: "flex", justifyContent: "center", width: "400px" }}>
      <Doughnut data={data} options={options} />
    </div>
  );
}

export default DoughnutChart;
