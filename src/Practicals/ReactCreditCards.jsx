import React, { useState } from "react";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";

function ReactCreditCards() {
  const [name, setName] = useState("second");
  const [number, setNumber] = useState("second");
  const [expiry, setExpiry] = useState("second");
  const [cvc, setCvc] = useState("second");
  const [focus, setFocus] = useState("second");
  return (
    <div>
      <Cards
        number={number}
        name={name}
        expiry={expiry}
        cvc={cvc}
        focused={focus}
      />
      <form>
        Number :
        <input
          type="tel"
          name="number"
          value={number}
          onChange={(e) => setNumber(e.target.value)}
          placeholder="Card Number"
          onFocus={(e) => setFocus(e.target.name)}
        ></input>
        Name :
        <input
          type="text"
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Card Holder Name"
          onFocus={(e) => setFocus(e.target.name)}
        ></input>
        Expiry :
        <input
          type="text"
          name="expiry"
          value={expiry}
          onChange={(e) => setExpiry(e.target.value)}
          placeholder="MM/YY Expiry"
          onFocus={(e) => setFocus(e.target.name)}
        ></input>
        CVC :
        <input
          type="tel"
          name="cvc"
          value={cvc}
          onChange={(e) => setCvc(e.target.value)}
          placeholder="CVC"
          onFocus={(e) => setFocus(e.target.name)}
        ></input>
      </form>
    </div>
  );
}

export default ReactCreditCards;
