import React from "react";
import { useIdleTimer } from "react-idle-timer";

function ReactIdleTimer() {
  const handleOnIdle = () => console.log("user is idle");
  const handleOnActive = () => console.log("system is active");
  const idleTimer = useIdleTimer({
    timeout: 2000,
    onIdle: handleOnIdle,
    onActive: handleOnActive,
  });
  return <></>;
}

export default ReactIdleTimer;
