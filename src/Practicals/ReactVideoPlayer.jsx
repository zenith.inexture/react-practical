import React from "react";
import ReactPlayer from "react-player";

function ReactVideoPlayer() {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <ReactPlayer
        width="480px"
        height="240px"
        controls
        url="https://www.youtube.com/watch?v=poGEVboh9Rw"
        // if we want to do someting on anyone of this event then it's provide the props, we have to pass the callback functin on that
        onReady={() => console.log("onReady callback")}
        onStart={() => console.log("onStart callback")}
        onPause={() => console.log("onPause callback")}
        onError={() => console.log("onError callback")}
        onEnded={() => console.log("onEnded callback")}
      />
    </div>
  );
}

export default ReactVideoPlayer;
