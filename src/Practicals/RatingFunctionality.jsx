import React, { useState } from "react";
import { FaStar } from "react-icons/fa";

const RatingFunctionality = () => {
  const [fullStarIndex, setFullStarIndex] = useState(0);
  const [fullRating, setFullRating] = useState(0);
  return (
    <div style={{ textAlign: "center" }}>
      {[...Array(10)].map((item, index) => (
        <span key={index + 1}>
          <FaStar
            color={fullStarIndex >= index + 1 ? "orange" : ""}
            size="50px"
            onClick={() => setFullRating(index + 1)}
            onMouseEnter={() => setFullStarIndex(index + 1)}
            onMouseLeave={() => setFullStarIndex(fullRating)}
            style={{ cursor: "pointer" }}
          />
        </span>
      ))}
      <span style={{ marginLeft: "20px" }}>{fullRating}/10</span>
    </div>
  );
};

export default RatingFunctionality;
