import React from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//this is custom toast notification
const CustomToast = ({ toastClose }) => {
  return (
    <div>
      Someting want wrong!
      <button onClick={toastClose} style={{ float: "right" }}>
        Close
      </button>
    </div>
  );
};

function ToastNotification() {
  const notify = () => {
    toast("Basic Notification!", { position: toast.POSITION.TOP_RIGHT });
    // second perameter is the position of the notification, autoClose notification etc...
    toast.success("Success Notification!", {
      position: toast.POSITION.TOP_LEFT,
      autoClose: 8000,
    });
    toast.error("Error Notification!", {
      position: toast.POSITION.TOP_CENTER,
      autoClose: false,
    });
    toast.warn(<CustomToast />, {
      position: toast.POSITION.BOTTOM_RIGHT,
      autoClose: false,
      closeButton: false, //remove the close button form the notification
    });
    // we can pass the customToast message notification for the dispaly also
    toast.info("Information Notification!", {
      position: toast.POSITION.BOTTOM_LEFT,
    });
    toast("Basic Notification!", { position: toast.POSITION.BOTTOM_CENTER });
  };
  return (
    <div>
      <button onClick={notify}>Notify!</button>
      <ToastContainer />
    </div>
  );
}

export default ToastNotification;
