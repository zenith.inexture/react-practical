import React, { useState } from "react";
import { ChromePicker } from "react-color";

function ReactColor() {
  //state for store the color value and change the color value
  const [color, setColor] = useState("#fff");
  //for toggle the color picker
  const [showColorPicker, setShowColorPicker] = useState(false);

  return (
    <div>
      <button onClick={() => setShowColorPicker(!showColorPicker)}>
        {showColorPicker ? "Close color picker" : "Pick a color"}
      </button>
      {showColorPicker && (
        <>
          {/* this component take perameter same as the input field element */}
          {/* we have to controll the color picker manually with onChange */}
          <ChromePicker
            color={color}
            onChange={(updatedColor) => setColor(updatedColor.hex)}
          />
          <h1>us picked - {color}</h1>
          <div
            style={{
              border: "1px solid black",
              width: "100px",
              height: "100px",
              backgroundColor: `${color}`,
            }}
          ></div>
        </>
      )}
    </div>
  );
}

export default ReactColor;
