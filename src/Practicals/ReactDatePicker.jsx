import React, { useState } from "react";
import DatePicker from "react-datepicker";

function ReactDatePicker() {
  const [selectedDate, setSelectedDate] = useState(new Date());
  return (
    <div style={{ textAlign: "center" }}>
      <DatePicker
        selected={selectedDate}
        onChange={(date) => setSelectedDate(date)}
        dateFormat="MM/dd/yyyy"
        // minDate={new Date()} //we can set the start and end date
        // maxDate={new Date()}

        //filter date props
        // filterDate={(date) => date.getDay() !== 6 && date.getDay() !== 0}

        //after clicking the date the clear button is comes and we can clear the selected date
        // isClearable={true}

        showYearDropdown={true}
      />
    </div>
  );
}

export default ReactDatePicker;
