import React from "react";
import BarChart from "./BarChart";
import DoughnutChart from "./DoughnutChart";
import LineChart from "./LineChart";

function ReactChart() {
  return (
    <div>
      <LineChart />
      <BarChart />
      <DoughnutChart />
    </div>
  );
}

export default ReactChart;
