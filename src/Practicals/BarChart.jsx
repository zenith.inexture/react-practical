import React from "react";
import { Chart as ChartJS, registerables } from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(...registerables);

function BarChart() {
  const data = {
    labels: ["jan", "fab", "mar", "apr", "may"],
    datasets: [
      {
        label: "Sales for 2020 (M)",
        data: [3, 2, 2, 1, 5],
        fill: true,
        tension: 0.5,
        borderColor: [
          "rgba(255, 0, 0, 0.6)",
          "rgba(0, 255, 0, 0.6)",
          "rgba(255, 255, 0, 0.6)",
          "rgba(0, 0, 255, 0.6)",
          "rgba(128, 0, 128, 0.6)",
        ],
        backgroundColor: [
          "rgba(255, 0, 0, 0.7)",
          "rgba(0, 255, 0, 0.7)",
          "rgba(255, 255, 0, 0.7)",
          "rgba(0, 0, 255, 0.7)",
          "rgba(128, 0, 128, 0.7)",
        ],
        hoverBackgroundColor: ["red", "green", "yellow", "blue", "purple"],
        hoverBorderColor: ["red", "green", "yellow", "blue", "purple"],
        borderWidth: 1,
        barThickness: 90,
        borderRadius: 5,
      },
    ],
  };

  const options = {
    title: {
      display: true,
      text: "Bar Chart",
    },
    scales: {
      yAxes: {
        min: 0,
        max: 7,
      },
    },
  };
  return (
    <div style={{ display: "flex", justifyContent: "center", width: "720px" }}>
      <Bar data={data} options={options} />
    </div>
  );
}

export default BarChart;
