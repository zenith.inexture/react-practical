import React from "react";
import CountUp from "react-countup";

function ReactCountUp() {
  return (
    <h1>
      <CountUp end={200} />+{/* default duration of this animation is 2sec */}
      <br></br>
      <CountUp end={200} duration={5} />+
      {/* duration of this countup is 5sec */}
      <br></br>
      <CountUp end={200} duration={5} start={50} />+
      {/* we can controll the start animation of this count up*/}
      <br></br>
      <CountUp end={1000} duration={5} start={500} prefix="$" decimals={2} />
      {/* we can write the start and the end of the counter*/}
      <br></br>
      <CountUp end={1000} duration={5} start={500} suffix="USD" decimals={2} />
    </h1>
  );
}

export default ReactCountUp;
