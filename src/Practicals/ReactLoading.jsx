import React from "react";
import { BarLoader, BeatLoader, BounceLoader } from "react-spinners";

function ReactLoading() {
  return (
    <div
      style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      {/* we can add some style with the emotion pkg */}
      <BounceLoader loading size={34} color="yellow" />
      <BarLoader loading size={25} color="orange" />
      <BeatLoader loading size={24} color="red" />
    </div>
  );
}

export default ReactLoading;
